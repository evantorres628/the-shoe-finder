# Written by: Evan Torres, 4/21/20 - 4/26/20

import validators
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import time

def linkURL(new, number, size, condition):
    URL = 'https://www.goat.com/sneakers/' + str(new) + '-' + str(number) + '/buy?boxCondition=' + str(condition) + '&size=' + str(size)
    return URL


name = input('Name of shoe: ').lower()
new = name.replace(' ', '-')

sku = input('SKU: ').lower()
number = sku.replace(' ', '-')

box = input('Box Condition: ').lower()
condition = box.replace(' ', '_')

size = input('Size: ')


URL = linkURL(new, number, size, condition)

valid=validators.url(URL)
if valid==True:
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get(str(URL))
    time.sleep(10)
    search_box = driver.find_element_by_name('q')
    search_box.send_keys('ChromeDriver')
    search_box.submit()
    time.sleep(10)
    content = driver.find_element_by_css_selector('p.GoatButton__Wrapper-sc-6hlaqt-0 jMBjaJ Hero__Button-kuugln-9 kjhqrt')
    print("Shoe available")

else:
    print("Unable to find shoe, please make sure the information you have typed in is correct.")
